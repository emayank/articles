from django.db import models
from django.contrib.auth.models import User


class UserProfileManager(models.Manager):
    def create_user_profile(self, user_id):
        self.create(user_id=user_id)


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    permissions = models.CharField(max_length=64, default='comment,')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = UserProfileManager()


class PostManager(models.Manager):
    def create_post(self, user_id, post_title, post_content, post_published):
        post = self.create(user_id=user_id, post_title=post_title, post_content=post_content, post_published=post_published)
        return post


class Post(models.Model):
    user = models.ForeignKey(User)
    post_title = models.CharField(max_length=1024)
    post_tags = models.CharField(max_length=256)
    post_content = models.TextField()
    post_published = models.CharField(max_length=8)
    post_category = models.CharField(max_length=16)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = PostManager()


class PinManager(models.Manager):
    def create_pin(self, user_id, post_id):
        self.create(user_id=user_id, post_id=post_id)


class Pin(models.Model):
    user = models.ForeignKey(User)
    post = models.ForeignKey(Post)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = PinManager()


class PhotoManager(models.Manager):
    def create_photo(self, user_id):
        photo = self.create(user_id=user_id)
        return photo


class Photo(models.Model):
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = PhotoManager()


class CommentManager(models.Manager):
    def create_comment(self, user_id, post_id, text):
        comment = self.create(user_id=user_id, post_id=post_id, text=text)
        return comment


class Comment(models.Model):
    user = models.ForeignKey(User)
    post = models.ForeignKey(Post)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    objects = CommentManager()
