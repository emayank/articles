from django.contrib.auth.models import User
from shutterdeck.models import UserProfile


def _can(user, permission):
    if user.is_authenticated():
        profile = UserProfile.objects.get(user_id=user.id)
        permissions = profile.permissions.split(',')
        return permission in permissions


def user_profile(request):
    profile = None
    profile_id = request.GET.get('profile_id')
    if profile_id is not None:
        profile = User.objects.get(id=profile_id)
    elif request.user.is_authenticated():
        profile = request.user
    can_user_post = _can(request.user, 'post')
    can_user_comment = _can(request.user, 'comment')
    return dict(profile=profile, can_user_post=can_user_post, can_user_comment=can_user_comment)
