from shutterdeck import views
from django.conf.urls import url, include

urlpatterns = [
    url(r'^$', views.index),

    url(r'^register/$', views.register),
    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),

    url(r'^home/$', views.home),

    url(r'^photo/upload/$', views.upload_photo),

    url(r'^post/edit/$', views.edit_post),
    url(r'^post/preview/$', views.preview_post),
    url(r'^post/save/$', views.save_post),
    url(r'^post/view/(?P<post_id>[-\w]+)/$', views.view_post),
    url(r'^post/list/(?P<profile_id>[-\w]+)$', views.list_post),
    url(r'^post/pin/(?P<post_id>[-\w]+)/$', views.pin_post),

    url(r'^profile/view/$', views.view_profile),
    url(r'^profile/save/$', views.save_profile),

    url('^categories/(?P<name>[-\w]+)/$', views.view_category),
    url(r'^comment/save/$', views.save_comment),

]
