var sdcdn = "https://s3.amazonaws.com/sdcdn"

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function showMessage(message) {
    $alert = $('<div class="alert alert-danger alert-dismissible" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>');
    $('#message').html($alert);
}

function registerHandler() {
    $('#register-btn').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $form = $(this).parents('form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                location.href = '/home';
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        });
        return false;
    });
}

function loginHandler() {
    $('#login-btn').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $form = $(this).parents('form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                location.href = '/home';
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
        return false;
    });
}

function profileHandler() {
    $('#profile-edit-save').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $form = $(this).parents('form');
        var $target = $(e.target);
        var data = $form.serializeObject();
        data = JSON.stringify(data);
        var url = $form.attr('action');
        var method = $form.attr('method');
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i);
        $.ajax({
            'url': url,
            'method': method,
            'contentType': 'application/json; charset=utf-8',
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
            $('#profile-modal').modal('hide');
        });
    });
}

function editHandler() {
    $('#post').markdownEditor({
        preview: true,
        onPreview: function (content, callback) {
            $.ajax({
                url: '/post/preview/',
                type: 'POST',
                dataType: 'html',
                data: {content: content},
            }).done(function(result) {
                  callback(result);
            });
        },
        imageUpload: true, // Activate the option
        uploadPath: '/photo/upload/' // Path of the server side script that receive the files
    });

    $('#post-save').on('click', function(e) {
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $target = $(e.target);
        var $post_id = $('#post-id');
        var post_title = $('#post-title').val();
        var post_content = $('#post').markdownEditor('content');
        var data = {'post_title': post_title, 'post_content': post_content, 'publish': 'false'};
        if ($post_id.length == 1) {
            data = {'post_id': $post_id.val(), 'post_title': post_title, 'post_content': post_content, 'publish': 'false'};
        }
        var url = '/post/save/';
        var method = 'post';
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                showMessage('Saved successfully');
                if (data.hasOwnProperty('post_id')) {
                    window.location.href = '/post/edit/?post_id=' + data.post_id;
                }
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
    });

    $('#post-publish').on('click', function(e) {
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $target = $(e.target);
        var $post_id = $('#post-id');
        var post_title = $('#post-title').val();
        var post_content = $('#post').markdownEditor('content');
        var data = {'post_title': post_title, 'post_content': post_content, 'publish': 'true'};
        if ($post_id.length == 1) {
            data = {'post_id': $post_id.val(), 'post_title': post_title, 'post_content': post_content, 'publish': 'true'};
        }
        var url = '/post/save/';
        var method = 'post';
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                showMessage('Saved successfully');
                if (data.hasOwnProperty('post_id')) {
                    window.location.href = '/post/edit/?post_id=' + data.post_id;
                }
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
    });
}

function pinHandler() {
    $('#post-pin').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $target = $(this);
        var post_id = $(this).attr('post_id');
        var url = '/post/pin/' + post_id + '/';
        var method = 'post';
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'dataType': 'json',
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                showMessage(data.message);
                window.location.reload();
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
    });
}

function commentHandler() {
    $('#comment').markdownEditor({
        preview: true,
        height: '200px',
        fullscreen: false,
        onPreview: function (content, callback) {
            $.ajax({
                url: '/post/preview/',
                type: 'POST',
                dataType: 'html',
                data: {content: content},
            }).done(function(result) {
                  callback(result);
            });
        },
    });
    $('#comment .btn-toolbar .btn-group').slice(0,4).hide();
    $('#comment-save').on('click', function(e){
        if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
        var $target = $(this);
        var post_id = $(this).attr('post_id');
        var post_comment = $('#comment').markdownEditor('content');
        var data = {post_id: post_id, post_comment: post_comment};
        var url = '/comment/save/';
        var method = 'post';
        var $i = $('<i/>');
        $i.addClass("fa fa-refresh fa-spin");
        $target.append($i)
        $.ajax({
            'url': url,
            'method': method,
            'dataType': 'json',
            'data': data
        }).done(function(data){
            $i.fadeOut('slow', function(){
                $i.remove();
            });
            if (data.status === 'success') {
                showMessage('Added comment');
                window.location.reload();
            } else if (data.status === 'error') {
                showMessage(data.message);
            }
        }).error(function(data){
            $i.remove();
        });
    });
}