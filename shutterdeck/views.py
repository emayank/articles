import os
import json
import boto3
import markdown
from articles import settings
from shutterdeck import helpers
from django.contrib import auth
from django.http import JsonResponse
from django.shortcuts import redirect, HttpResponse, render
from common.response import Renderer, Success, Error
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from shutterdeck.models import Pin
from shutterdeck.models import Post
from shutterdeck.models import Photo
from shutterdeck.models import Comment
from shutterdeck.models import UserProfile


def index(request):
    if request.user.is_authenticated():
        return redirect('/home')
    return Renderer.render(request, 'index.html')


# @csrf_protect
def register(request):
    return Error()
    if request.method == 'GET':
        return Renderer.render(request, 'register.html')
    elif request.method == 'POST' and request.is_ajax():
        params = request.body.decode()
        params = json.loads(params)
        user = helpers.create_user(params)
        # UserProfile.objects.create_user_profile(user.id)
        return Success()


@ensure_csrf_cookie
def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')
        # return Renderer.render(request, 'login.html')
    elif request.method == 'POST' and request.is_ajax():
        params = json.loads(request.body.decode())
        user = helpers.login(params)
        auth.login(request, user)
        return Success()


def logout(request):
    if request.method == 'GET':
        auth.logout(request)
        return redirect('/')


def home(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            pins = request.user.pin_set.all()
            posts = [pin.post for pin in pins]
            return _listing(request, posts, False)
        else:
            return redirect('/')


s3 = boto3.resource("s3")


def _upload_to_s3(request, file_):
    dirname, basename = os.path.split(file_)
    upload_as = os.path.join('articles', request.user.id, basename)
    s3.Object(bucket_name="sdcdn", key=upload_as).put(Body=open(file_, "rb"), ACL="public-read")
    return os.path.join(settings.CDN_BUCKET, upload_as)


@login_required
def upload_photo(request):
    if request.method == 'POST' and 'name' in request.FILES:
        uploaded = []
        for k, f in request.FILES:
            photo = Photo.objects.create_photo(user_id=request.user.id)
            file_ = '/var/uploads/articles/' + photo.id
            with open(file_, 'wb+') as image:
                for chunk in f.chunks():
                    image.write(chunk)
            uploaded.append(_upload_to_s3(request, file_))
        return JsonResponse(uploaded, safe=False)


def _can(user, permission):
    if user.is_authenticated():
        user_profile = UserProfile.objects.get(user_id=user.id)
        permissions = user_profile.permissions.split(',')
        return permission in permissions


@login_required
def edit_post(request):
    can_user_post = _can(request.user, 'post')
    if not can_user_post:
        return redirect('/')
    if request.method == 'GET':
        post = None
        post_id = request.GET.get('post_id')
        if post_id is not None:
            post = Post.objects.get(id=post_id)
        context = dict(post=post)
        return Renderer.render(request, 'editor.html', context)
    elif request.method == 'POST':
        pass


def _parse_markdown(content):
    return markdown.markdown(content, extensions=['codehilite'])


@login_required
def preview_post(request):
    if request.is_ajax() and request.method == 'POST':
        content = request.POST.get('content', '**Error**')
        preview = _parse_markdown(content)
        return HttpResponse(preview)


def view_post(request, post_id):
    pin = None
    if request.user.is_authenticated():
        pin = Pin.objects.filter(post_id=post_id, user_id=request.user.id)
    if request.method == 'GET':
        post = Post.objects.get(id=post_id)
        comments = post.comment_set.all().order_by('-created_at')[0:10]
        for comment in comments:
            comment.text = _parse_markdown(comment.text)
        post.post_content = _parse_markdown(post.post_content)
        context = dict(post=post, comments=comments, pin=pin)
        return Renderer.render(request, 'post.html', context)


# @csrf_protect
@login_required
def save_post(request):
    can_user_post = _can(request.user, 'post')
    if not can_user_post:
        return Error()
    if request.method == 'POST':
        post_id = request.POST.get('post_id')
        post_content = request.POST.get('post_content', '')
        post_title = request.POST.get('post_title', '')
        post_published = request.POST.get('publish', 'false')
        if post_id is None:
            post = Post.objects.create_post(request.user.id, post_title, post_content, post_published)
            return Success(post_id=post.id)
        else:
            post = Post.objects.filter(id=post_id)
            post.update(post_title=post_title, post_content=post_content, post_published=post_published)
            return Success()


# @login_required
def list_post(request, profile_id):
    if request.method == 'GET':
        posts = Post.objects.filter(user_id=profile_id)
        return _listing(request, posts)


# @csrf_protect
@login_required
def pin_post(request, post_id):
    if request.method == 'POST' and request.user.is_authenticated():
        pin, created = Pin.objects.get_or_create(user_id=request.user.id, post_id=post_id)
        if not created:
            pin.delete()
            return Success(message='Pin deleted. You can go to your homepage to check your pinned posts')
        return Success(message='Pinned successfully. You can go to your homepage to check your pinned posts')
    return Error(message='Not Authorised')


def view_profile(request):
    if request.method == 'GET':
        profile_id = request.GET.get('profile_id')
        if profile_id is None and not request.user.is_authenticated():
            return redirect('/login')
        return Renderer.render(request, 'profile.html')


# @csrf_protect
@login_required
def save_profile(request):
    pass


def _listing(request, posts, share=True):
    paginator = Paginator(posts, 10)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    context = dict(posts=posts, share=share)
    return Renderer.render(request, 'summary.html', context)


def view_category(request, name):
    if request.method == 'GET':
        # posts = Post.objects.filter(post_category=name)
        posts = Post.objects.all()
        return _listing(request, posts)


# @csrf_protect
@login_required
def save_comment(request):
    if request.method == 'POST':
        post_id = request.POST.get('post_id')
        post_comment = request.POST.get('post_comment')
        comment = Comment.objects.create_comment(request.user.id, post_id, post_comment)
        return Success()
    return Error(message='Not Authorised')
