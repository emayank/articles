from django.dispatch import receiver
from shutterdeck.models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.db.models.signals import post_save


def create_user(params):
    email = params.get('email')
    first_name = params.get('first_name')
    last_name = params.get('last_name')
    password = params.get('password')
    user = User.objects.create_user(email, email, password, first_name=first_name, last_name=last_name)
    return user


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, *args, **kwargs):
    if created:
        user = instance
        UserProfile.objects.create_user_profile(user.id)


def login(params):
    email = params.get('email')
    password = params.get('password')
    user = authenticate(username=email, password=password)
    if user is None:
        raise Exception("The username/password combination doesn't seem to be correct")
    if not user.is_active:
        raise Exception("This user is not Active")
    return user
