from django.shortcuts import render
from django.http import JsonResponse
from django.template.context_processors import csrf


class Response(JsonResponse):
    def __init__(self, **kwargs):
        super(Response, self).__init__(kwargs)


class Success(Response):
    def __init__(self, **kwargs):
        kwargs.update(status="success")
        super(Success, self).__init__(**kwargs)


class Error(Response):
    def __init__(self, **kwargs):
        kwargs.update(status="error")
        super(Error, self).__init__(**kwargs)


class Renderer:
    @staticmethod
    def render(request, template_name, context=None):
        if context is None:
            context = {}
        context.update(csrf(request))  # Cookie name csrftoken
        return render(request, template_name, context)
